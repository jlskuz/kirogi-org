---
layout: page
title: Download
sorted: 4
---

## Download

Kirogi development is still in its early stages. While already usable, the project has not issued a stable release yet.

Pre-release builds and source code are available below.

We expect developer- and enthusiast-oriented software distributions to pick up Kirogi soon. We'll update this page with additional links as we are informed of them.

# Nightly Development Builds

<b>NOTE:</b> Due to a firmware bug, viewing video from the Parrot Anafi requires GStreamer 1.16.1, which incorporates a workaround we developed. GStreamer 1.16.1 has not been released yet and is not yet included in the Flatpak or Android builds.

## Linux (Flatpak x86-64 &amp; ARM)

Nightly Flatpak builds for desktop Linux are available via KDE's <a href="https://community.kde.org/Guidelines_and_HOWTOs/Flatpak">development Flatpak repository</a>.

To install a Flatpak build, click <a href="https://distribute.kde.org/kdeapps.flatpakrepo">here</a> to add the repository to your software center (for example <a href="https://userbase.kde.org/Discover">Plasma Discover</a>) and search for Kirogi.

Alternatively, you can run the following commands in a terminal:

```
flatpak remote-add --if-not-exists kdeapps --from https://distribute.kde.org/kdeapps.flatpakrepo
flatpak install kdeapps org.kde.kirogi
```

## Android

Nightly APKs for Android 5.0+ are available <a href="https://binary-factory.kde.org/job/Kirogi_android/lastBuild/">here</a> and soon via KDE's experimental <a href="https://community.kde.org/Android/FDroid">F-Droid repository.</a>

# Source Code

You can browse (and find <code>git clone</code> URLs for) Kirogi's source code <a href="https://invent.kde.org/kde/kirogi">here</a>.

# Assets

You can find additional assets for the Kirogi project (including the hi-res version of our mascot) in [this folder](https://share.kde.org/s/ierTeeQWNgmSckH) on share.kde.org.

# Other Resources

On September 8th, 2019, Eike Hein gave a talk introducing Kirogi at KDE's [Akademy](https://akademy.kde.org/2019/) conference. The slides are available [here](https://share.kde.org/s/aZn6KyqoPioaRAF) and a video [here](https://files.kde.org/akademy/2019/102-Taking_KDE_to_the_skies_Making_the_drone_ground_control_Kirogi.mp4).

During early development, we published several videos showing off progress. You can find a playlist [here](https://www.youtube.com/playlist?list=PLhDLD8D_uzJWrrynXYbqdy7jGTRgBDpSY).
