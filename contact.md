---
layout: page
title: Contact
sorted: 6
---

# Bug Tracker

To report a bug or request a new feature or enhancement, you can use [this form](https://bugs.kde.org/enter_bug.cgi?product=kirogi) in KDE's bug tracker.

# Chat

Kirogi has a group chat for both users and developers to communicate in. You can join the discussion via Matrix, IRC or Telegram. All three are bridged, so you will see the same messages using any of these services.

For Matrix, you can use KDE's official web chat to join [`#kde-kirogi:kde.org`](https://webchat.kde.org/#/room/#kirigami:kde.org), or any other Matrix client. More information on KDE's Matrix instance is available [here](https://community.kde.org/Matrix).

For IRC, see [`#kde-kirogi` on freenode](irc://chat.freenode.net/kde-kirogi) (you can join via freenode's official [web chat](https://webchat.freenode.net/#kde-kirogi) as well).

For Telegram, use this [invite link](https://t.me/kde_kirogi) to join the @kde_kirogi group.